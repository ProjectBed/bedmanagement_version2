<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Post;
use Illuminate\Http\Request;

class ReservationController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $posts = Post::where('title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $posts = Post::latest()->paginate($perPage);
        }

        return view('reservation', compact('posts'));
    }


    public function create()
    {
        return view('reservationCreate');
    }


    public function store(Request $request)
    {

        $requestData = $request->all();
        Post::create($requestData);

        return redirect('reservation')->with('flash_message', 'Post added!');
    }


    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('reservationShow', compact('post'));
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);

        return view('reservationEdit', compact('post'));
    }

    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $post = Post::findOrFail($id);
        $post->update($requestData);

        return redirect('reservation')->with('flash_message', 'Post updated!');
    }

    public function destroy($id)
    {
        Post::destroy($id);

        return redirect('reservation')->with('flash_message', 'Post deleted!');
    }
}
